import React, { useEffect, useState } from 'react';

import { CircularProgress, Box, Typography } from '@material-ui/core';

import DashboardList from '../../components/DashboardList';
import { ProviderClient } from '../../context/clientList';
import { getClients } from '../../services/clients/get';
import { convertClientRequestToClientList, Client } from '../../utils/convert';

const Dashboard: React.FC = () => {
  const [clientsArray, setClientsArray] = useState<Array<Client>>([]);
  const [loading, setLoading] = useState(true);
  const [showTable, setShowTable] = useState(false);
  const fetchClients = async () => {
    const data = await getClients();
    const arrayClients = convertClientRequestToClientList(data);
    if (arrayClients.length === 0) {
      setShowTable(false);
    } else {
      setShowTable(true);
    }

    setClientsArray(arrayClients);

    setLoading(false);
  };

  useEffect(() => {
    try {
      setLoading(true);
      fetchClients();
    } catch (e) {
      setLoading(false);
    }
  }, []);

  return (
    <ProviderClient list={clientsArray} setList={setClientsArray}>
      {showTable ? (
        <DashboardList />
      ) : (
        <>
          {loading ? (
            <Box display="flex" justifyContent="center" mt={4}>
              <CircularProgress />
            </Box>
          ) : (
            <Box display="flex" justifyContent="center" mt={4}>
              <Typography>Ainda não há clientes cadastrados</Typography>
            </Box>
          )}
        </>
      )}
    </ProviderClient>
  );
};

export default Dashboard;
