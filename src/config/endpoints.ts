export default {
  users: {
    login: 'usuarios',
  },
  client: {
    addClient: 'clientes/',
    getClients: 'clientes/',
    deleteClient: (id: number): string => `clientes/${id}`,
    editClient: (id: number): string => `clientes/${id}`,
  },
};
