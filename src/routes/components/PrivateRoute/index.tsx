import React from 'react';

import { Route, Redirect, RouteProps } from 'react-router-dom';

import { useAuth } from '../../../hooks/useAuth';

const PrivateRoute: React.FC<RouteProps> = ({ children, ...rest }) => {
  const auth = useAuth();
  return (
    <Route
      {...rest}
      render={() =>
        auth.token ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/login',
            }}
          />
        )
      }
    />
  );
};

export default PrivateRoute;
