import React, { useState } from 'react';

import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/core/styles';

import { ProviderAuth } from './context/auth';
import { ProviderTheme } from './context/theme';
import Routes from './routes';
import { theme } from './themes/theme';

function App(): JSX.Element {
  const [isDarkTheme, toggleTheme] = useState(
    localStorage.getItem('theme') === 'dark'
  );
  const [token, setToken] = useState(localStorage.getItem('token') || '');

  return (
    <ProviderTheme isDarkTheme={isDarkTheme} toggleTheme={toggleTheme}>
      <ThemeProvider theme={theme(isDarkTheme)}>
        <CssBaseline />
        <ProviderAuth token={token} setToken={setToken}>
          <Routes />
        </ProviderAuth>
      </ThemeProvider>
    </ProviderTheme>
  );
}

export default App;
