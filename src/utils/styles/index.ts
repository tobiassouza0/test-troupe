import { makeStyles } from '@material-ui/core/styles';

export const utilsStyles = makeStyles(() => ({
  removeTextDecoration: {
    textDecoration: 'none',
  },
  removeMinWidth: {
    minWidth: 'auto',
  },
  removePadding: {
    padding: 0,
  },
}));
