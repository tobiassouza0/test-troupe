import { Client as RequestClient } from '../../services/clients/get';

export interface Client {
  name: string;
  cpf: string;
  email: string;
  city: string;
  id: number;
  cep: number;
  rua: string;
  numero: number;
  bairro: string;
}

export const convertClientRequestToClientList = (
  client: Array<RequestClient>
): Array<Client> => {
  return client.map((item) => {
    return {
      name: item.nome,
      cpf: item.cpf,
      email: item.email,
      city: item.endereco.cidade,
      id: item.id,
      cep: item.endereco.cep,
      rua: item.endereco.rua,
      numero: item.endereco.numero,
      bairro: item.endereco.bairro,
    };
  });
};

export const convertCepMaskOnString = (cep: string): string => {
  return parseInt(cep.split('-').join(''), 10).toString();
};
