import React from 'react';

import { TextField } from '@material-ui/core';
import { FormikProps } from 'formik';
import InputMask from 'react-input-mask';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
interface TextInputMaskProps<T = any> {
  name: string;
  placeholder: string;
  label?: string;
  formik: FormikProps<T>;
  mask: string;
  numericKeyboard?: boolean;
  variant?: 'standard' | 'filled' | 'outlined';
}

const TextInputMask: React.FC<TextInputMaskProps> = ({
  name,
  placeholder,
  label,
  formik,
  mask,
  numericKeyboard,
  variant,
}) => {
  return (
    <InputMask
      mask={mask}
      onChange={formik.handleChange}
      onBlur={formik.handleBlur}
      value={formik.values[name]}
    >
      {() => (
        <TextField
          id={name}
          name={name}
          label={label}
          placeholder={placeholder}
          fullWidth
          variant={variant || 'standard'}
          type={numericKeyboard ? 'tel' : 'text'}
          error={formik.touched[name] && Boolean(formik.errors[name])}
          helperText={formik.touched[name] && formik.errors[name]}
        />
      )}
    </InputMask>
  );
};

export default TextInputMask;
