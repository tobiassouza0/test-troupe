import React, { useContext, useRef } from 'react';

import { Box, TextField } from '@material-ui/core';

import { ClientContext } from '../../context/clientList';
import { getClientsByName } from '../../services/clients/get';
import { convertClientRequestToClientList } from '../../utils/convert';

const Cell: React.FC = () => {
  const { setList } = useContext(ClientContext);

  const inputRef = useRef<HTMLInputElement>(null);

  const changed = async () => {
    const name = inputRef.current?.value || '';
    try {
      const data = await getClientsByName(name);
      const arrayClients = convertClientRequestToClientList(data);
      setList(arrayClients);
    } catch (e) {
      setList([]);
    }
  };

  return (
    <Box display="flex" justifyContent="flex-end">
      <TextField
        placeholder="pesquise..."
        variant="outlined"
        onKeyUp={changed}
        inputRef={inputRef}
      />
    </Box>
  );
};

export default Cell;
