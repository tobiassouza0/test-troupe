import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(({ spacing }) => ({
  card: {
    display: 'flex',
    margin: `${spacing(2)}px 0px`,
    padding: spacing(1),
    alignItems: 'center',
  },
  buttonUp: {
    marginBottom: spacing(1),
    width: '100%',
  },
  buttonDown: {
    width: '100%',
  },
}));
