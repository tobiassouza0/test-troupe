import React, { useState, SyntheticEvent, useContext } from 'react';

import {
  Card,
  Grid,
  Button,
  Snackbar,
  SnackbarCloseReason,
} from '@material-ui/core';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import SaveOutlinedIcon from '@material-ui/icons/SaveOutlined';
import { Alert } from '@material-ui/lab';
import { useFormik } from 'formik';

import { ClientContext } from '../../context/clientList';
import { deleteClient } from '../../services/clients/delete';
import { editClient } from '../../services/clients/edit';
import { getClients } from '../../services/clients/get';
import { convertClientRequestToClientList } from '../../utils/convert';
import Cell from '../CellTable';

import { useStyles } from './styles';
import { validationSchema } from './validationSchema';

interface IProps {
  name: string;
  cpf: string;
  email: string;
  city: string;
  id: number;
  cep: number;
  rua: string;
  numero: number;
  bairro: string;
}

interface FeedbackState {
  open: boolean;
  type: 'error' | 'success';
  message: string;
}

const RowDashboardTable: React.FC<IProps> = ({
  name,
  cpf,
  email,
  city,
  id,
  cep,
  rua,
  numero,
  bairro,
}) => {
  const { setList } = useContext(ClientContext);
  const classes = useStyles();

  const [edit, setEdit] = useState(false);

  const [feedback, setFeedback] = useState<FeedbackState>({
    open: false,
    type: 'error',
    message: '',
  });

  const changeClientsTable = async () => {
    const data = await getClients();
    const arrayClients = convertClientRequestToClientList(data);
    setList(arrayClients);
  };

  const formik = useFormik({
    initialValues: {
      name,
      email,
      cpf,
      city,
    },
    validationSchema,
    onSubmit: async (values) => {
      try {
        const clientData = {
          nome: values.name,
          cpf: values.cpf,
          email: values.email,
          endereco: {
            cep,
            rua,
            numero,
            bairro,
            cidade: values.city,
          },
          id,
        };
        await editClient(clientData);
        setFeedback({
          open: true,
          type: 'success',
          message: 'Cliente salvo com sucesso',
        });
        await changeClientsTable();
      } catch (e) {
        setFeedback({
          open: true,
          type: 'error',
          message: e.message,
        });
      }

      setEdit(false);
    },
    validateOnBlur: true,
  });

  const onClickDelete = async () => {
    try {
      await deleteClient(id);
      setFeedback({
        open: true,
        type: 'success',
        message: 'Cliente apagado com sucesso',
      });
      await changeClientsTable();
    } catch (e) {
      setFeedback({
        open: true,
        type: 'error',
        message: e.message,
      });
    }
  };

  const handleClose = (
    event: SyntheticEvent<Element, Event>,
    reason: SnackbarCloseReason
  ) => {
    if (reason === 'clickaway') {
      return;
    }

    setFeedback({ ...feedback, open: false });
  };

  return (
    <form onSubmit={formik.handleSubmit}>
      <Snackbar
        open={feedback.open}
        autoHideDuration={6000}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      >
        <Alert severity={feedback.type}>{feedback.message}</Alert>
      </Snackbar>
      <Card className={classes.card}>
        <Grid container spacing={3} justifyContent="space-between">
          <Grid item xs={6} sm={2}>
            <Cell
              title="name"
              text={name}
              edit={edit}
              name="name"
              placeholder="Nome"
              label="Nome"
              formik={formik}
            />
          </Grid>
          <Grid item xs={6} sm={2}>
            <Cell
              title="CPF"
              text={cpf}
              edit={edit}
              name="cpf"
              placeholder="CPF"
              label="CPF"
              mask="999.999.999-99"
              formik={formik}
            />
          </Grid>
          <Grid item xs={6} sm={2}>
            <Cell
              title="Email"
              text={email}
              edit={edit}
              name="email"
              placeholder="Email"
              label="Email"
              formik={formik}
            />
          </Grid>
          <Grid item xs={6} sm={2}>
            <Cell
              title="Cidade"
              text={city}
              edit={edit}
              name="city"
              placeholder="Cidade"
              label="Cidade"
              formik={formik}
            />
          </Grid>
          <Grid item xs={12} sm={2}>
            {edit && (
              <Button
                variant="contained"
                color="default"
                className={classes.buttonUp}
                type="submit"
              >
                <SaveOutlinedIcon />
                Salvar
              </Button>
            )}
            {!edit && (
              <Button
                variant="contained"
                color="primary"
                className={classes.buttonUp}
                hidden={!edit}
                onClick={(e) => {
                  e.preventDefault();
                  setEdit(true);
                }}
              >
                <EditOutlinedIcon />
                Editar
              </Button>
            )}
            <Button
              variant="contained"
              color="secondary"
              className={classes.buttonDown}
              onClick={onClickDelete}
            >
              <DeleteOutlineOutlinedIcon />
              Excluir
            </Button>
          </Grid>
        </Grid>
      </Card>
    </form>
  );
};

export default RowDashboardTable;
