import { isValidCPF } from '@brazilian-utils/brazilian-utils';
import * as yup from 'yup';

export const validationSchema = yup.object({
  email: yup
    .string()
    .email('Necessário um email válido')
    .required('O email é obrigatório'),
  name: yup.string().required('O nome é obrigatório'),
  cpf: yup
    .string()
    .required('O CPF é obrigatório')
    .test('cpf', 'CPF inválido', (value) =>
      value ? isValidCPF(value) : false
    ),
  city: yup.string().required('Cidade é obrigatória'),
});
