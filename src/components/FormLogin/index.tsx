import React, { useState, SyntheticEvent } from 'react';

import {
  Card,
  Typography,
  Container,
  Box,
  TextField,
  Button,
  Snackbar,
  SnackbarCloseReason,
} from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { useFormik } from 'formik';
import { useHistory } from 'react-router-dom';

import { useAuth } from '../../hooks/useAuth';
import { getUser } from '../../services/auth/login';

import { useStyles } from './styles';
import { validationSchema } from './validationSchema';

interface FeedbackState {
  open: boolean;
  type: 'error' | 'success';
  message: string;
}

const FormLogin: React.FC = () => {
  const classes = useStyles();
  const { setToken } = useAuth();
  const history = useHistory();

  const [feedback, setFeedback] = useState<FeedbackState>({
    open: false,
    type: 'error',
    message: '',
  });

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema,
    onSubmit: async ({ email, password }) => {
      try {
        const user = await getUser();
        if (email === user.email && password === user.password) {
          setToken('token');
          history.push('/');
        } else {
          setFeedback({
            open: true,
            type: 'error',
            message: 'Credenciais inválidas',
          });
        }
      } catch (e) {
        setFeedback({
          open: true,
          type: 'error',
          message: e.message,
        });
      }
    },
    validateOnBlur: true,
  });

  const handleClose = (
    event: SyntheticEvent<Element, Event>,
    reason: SnackbarCloseReason
  ) => {
    if (reason === 'clickaway') {
      return;
    }

    setFeedback({ ...feedback, open: false });
  };

  return (
    <Container disableGutters maxWidth="xs">
      <Snackbar
        open={feedback.open}
        autoHideDuration={6000}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      >
        <Alert severity={feedback.type}>{feedback.message}</Alert>
      </Snackbar>
      <Card className={classes.form}>
        <Box p={2}>
          <Typography
            variant="h5"
            align="center"
            color="secondary"
            className={classes.formTitle}
          >
            Acesse sua conta
          </Typography>
          <form onSubmit={formik.handleSubmit}>
            <TextField
              id="email"
              name="email"
              placeholder="E-mail"
              fullWidth
              variant="outlined"
              className={classes.textField}
              value={formik.values.email}
              onChange={formik.handleChange}
              error={formik.touched.email && Boolean(formik.errors.email)}
              helperText={formik.touched.email && formik.errors.email}
              onBlur={formik.handleBlur}
            />
            <TextField
              id="password"
              name="password"
              placeholder="Senha"
              type="password"
              fullWidth
              variant="outlined"
              className={classes.textField}
              value={formik.values.password}
              onChange={formik.handleChange}
              error={formik.touched.password && Boolean(formik.errors.password)}
              helperText={formik.touched.password && formik.errors.password}
              onBlur={formik.handleBlur}
            />
            <Button
              className={classes.submitButton}
              color="secondary"
              variant="contained"
              fullWidth
              type="submit"
              disabled={!formik.isValid || !formik.dirty}
            >
              Acessar
            </Button>
          </form>
        </Box>
      </Card>
    </Container>
  );
};

export default FormLogin;
