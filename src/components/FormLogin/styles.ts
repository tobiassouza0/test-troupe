import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(({ spacing, palette }) => ({
  textField: {
    marginTop: spacing(1),
    marginBottom: spacing(1),
  },
  submitButton: {
    color: palette.grey[50],
    marginTop: spacing(2),
  },
  form: {
    marginTop: spacing(10),
  },
  formTitle: {
    marginBottom: spacing(3),
  },
  recoveryPasswordLink: {
    color: palette.primary.main,
    textDecoration: 'none',
    display: 'block',
    marginTop: spacing(2),
  },
}));
