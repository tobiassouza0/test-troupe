import * as yup from 'yup';

export const validationSchema = yup.object({
  email: yup
    .string()
    .email('Necessário um email válido')
    .required('O email é obrigatório'),
  password: yup
    .string()
    .min(4, 'A senha precisa de pelo menos 4 caracteres')
    .required('A senyha é obrigatória'),
});
