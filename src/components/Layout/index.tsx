import React from 'react';

import Container from '@material-ui/core/Container';

import Header from '../Header';

const components: React.FC = ({ children }) => {
  return (
    <>
      <Header />
      <Container maxWidth="md">
        <div>{children}</div>
      </Container>
    </>
  );
};

export default components;
