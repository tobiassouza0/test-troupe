import React, { useState, SyntheticEvent, useEffect, useRef } from 'react';

import {
  Container,
  Grid,
  Button,
  Snackbar,
  SnackbarCloseReason,
} from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { useFormik } from 'formik';

import { getAddressData } from '../../services/cep/get';
import { addClient } from '../../services/clients/add';
import { convertCepMaskOnString } from '../../utils/convert';
import TextInput from '../TextInput';
import TextInputMask from '../TextInputMask';

import { useStyles } from './styles';
import { validationSchema } from './validationSchema';

interface FeedbackState {
  open: boolean;
  type: 'error' | 'success';
  message: string;
}

const FormProfile: React.FC = () => {
  const classes = useStyles();

  const inputRef = useRef<HTMLInputElement>(null);

  const [feedback, setFeedback] = useState<FeedbackState>({
    open: false,
    type: 'error',
    message: '',
  });

  const formik = useFormik({
    initialValues: {
      nome: '',
      cpf: '',
      email: '',
      cep: '',
      rua: '',
      numero: '',
      bairro: '',
      cidade: '',
    },
    validationSchema,
    onSubmit: async ({
      nome,
      cpf,
      email,
      cep,
      rua,
      numero,
      bairro,
      cidade,
    }) => {
      try {
        await addClient({
          nome,
          cpf,
          email,
          endereco: {
            cep: parseInt(cep, 10),
            rua,
            numero: parseInt(numero, 10),
            bairro,
            cidade,
          },
        });
        formik.resetForm();
        await setFeedback({
          open: true,
          type: 'success',
          message: 'Cliente adicionado com sucesso',
        });
      } catch (e) {
        setFeedback({
          open: true,
          type: 'error',
          message: e.message,
        });
      }
    },
    validateOnBlur: true,
  });

  useEffect(() => {
    const cep = convertCepMaskOnString(formik.values.cep);
    if (cep.length === 8) {
      setAddress(cep);
    }
  }, [formik.values.cep]);

  const setAddress = async (cep: string) => {
    try {
      const address = await getAddressData(cep);
      if (address.erro) {
        formik.setValues({
          ...formik.values,
          rua: '',
          cidade: '',
          bairro: '',
        });
      } else {
        formik.setValues({
          ...formik.values,
          rua: address.logradouro,
          cidade: address.localidade,
          bairro: address.bairro,
        });
        inputRef.current?.focus();
      }
    } catch (e) {
      console.log(e);
    }
  };

  const handleClose = (
    event: SyntheticEvent<Element, Event>,
    reason: SnackbarCloseReason
  ) => {
    if (reason === 'clickaway') {
      return;
    }

    setFeedback({ ...feedback, open: false });
  };

  return (
    <Container disableGutters maxWidth="xs" className={classes.container}>
      <Snackbar
        open={feedback.open}
        autoHideDuration={6000}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      >
        <Alert severity={feedback.type}>{feedback.message}</Alert>
      </Snackbar>
      <form onSubmit={formik.handleSubmit}>
        <Grid container spacing={1} justifyContent="center">
          <Grid item xs={12}>
            <TextInput
              name="nome"
              placeholder="Nome"
              label="Nome"
              formik={formik}
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12}>
            <TextInputMask
              mask="999.999.999-99"
              name="cpf"
              placeholder="CPF"
              label="CPF"
              formik={formik}
              numericKeyboard
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12}>
            <TextInput
              name="email"
              placeholder="E-mail"
              label="Email"
              formik={formik}
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12}>
            <TextInputMask
              mask="99999-999"
              name="cep"
              placeholder="CEP"
              label="CEP"
              formik={formik}
              numericKeyboard
              variant="outlined"
            />
          </Grid>
          <Grid item xs={8}>
            <TextInput
              name="rua"
              placeholder="Rua"
              label="Rua"
              formik={formik}
              variant="outlined"
            />
          </Grid>
          <Grid item xs={4}>
            <TextInput
              name="numero"
              label="Nº"
              placeholder="Nº"
              formik={formik}
              variant="outlined"
              inputRef={inputRef}
            />
          </Grid>
          <Grid item xs={12}>
            <TextInput
              name="bairro"
              placeholder="Bairro"
              label="Bairro"
              variant="outlined"
              formik={formik}
            />
          </Grid>
          <Grid item xs={12}>
            <TextInput
              name="cidade"
              placeholder="Cidade"
              label="Cidade"
              formik={formik}
              variant="outlined"
            />
          </Grid>

          <Button className={classes.button} variant="contained" type="submit">
            Salvar
          </Button>
        </Grid>
      </form>
    </Container>
  );
};

export default FormProfile;
