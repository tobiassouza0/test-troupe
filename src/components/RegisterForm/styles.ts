import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(({ spacing, palette }) => ({
  container: {
    marginTop: spacing(4),
  },
  avatarLarge: {
    width: spacing(12),
    height: spacing(12),
  },
  button: {
    height: spacing(7),
    background: palette.primary.main,
    color: palette.grey[50],
    textTransform: 'none',
    width: '100%',
    marginTop: spacing(2),

    '&:hover': {
      background: palette.primary.dark,
    },
  },
}));
