import { isValidCPF, isValidCEP } from '@brazilian-utils/brazilian-utils';
import * as yup from 'yup';

export const validationSchema = yup.object({
  nome: yup.string().required('O nome é obrigatório'),
  cpf: yup
    .string()
    .required('O CPF é obrigatório')
    .test('cpf', 'CPF inválido', (value) =>
      value ? isValidCPF(value) : false
    ),
  email: yup
    .string()
    .email('Necessário um email válido')
    .required('O email é obrigatório'),
  cep: yup
    .string()
    .required('O CEP é obrigatório')
    .test('cep', 'CEP inválido', (value) =>
      value ? isValidCEP(value) : false
    ),

  rua: yup.string().required('Rua é obrigatória'),
  numero: yup.string().required('Número é Obrigatório'),
  bairro: yup.string().required('O bairro é obrigatório'),
  cidade: yup.string().required('Cidade é obrigatória'),
});
