import React, { Ref } from 'react';

import { TextField } from '@material-ui/core';
import { FormikProps } from 'formik';

interface TextInputProps<T> {
  name: string;
  label?: string;
  placeholder: string;
  formik: FormikProps<T>;
  disabled?: boolean;
  variant?: 'standard' | 'filled' | 'outlined';
  inputRef?: Ref<HTMLInputElement>;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const TextInput: React.FC<TextInputProps<any>> = ({
  formik,
  label,
  placeholder,
  name,
  disabled,
  variant,
  inputRef,
}) => {
  return (
    <TextField
      id={name}
      name={name}
      label={label}
      placeholder={placeholder}
      fullWidth
      variant={variant || 'standard'}
      value={formik.values[name]}
      onChange={formik.handleChange}
      error={formik.touched[name] && Boolean(formik.errors[name])}
      helperText={formik.touched[name] && formik.errors[name]}
      onBlur={formik.handleBlur}
      disabled={disabled}
      inputRef={inputRef}
    />
  );
};

export default TextInput;
