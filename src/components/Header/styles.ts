import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(({ spacing }) => ({
  header: {
    marginTop: spacing(3),
  },
  logo: {
    width: spacing(9),
  },
  toggleThemeButton: {
    marginLeft: 'auto',
    marginRight: spacing(1),
  },
  menuOutlinedButton: {
    order: 1,
  },
}));
