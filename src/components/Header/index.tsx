import React, { useState } from 'react';

import {
  IconButton,
  AppBar,
  Container,
  Toolbar,
  Typography,
  Box,
  Hidden,
  Button,
  Link as LinkStyle,
} from '@material-ui/core';
import MoonIcon from '@material-ui/icons/Brightness3Outlined';
import SunIcon from '@material-ui/icons/Brightness5Outlined';
import ExitToAppOutlinedIcon from '@material-ui/icons/ExitToAppOutlined';
import MenuOutlinedIcon from '@material-ui/icons/MenuOutlined';
import { Link } from 'react-router-dom';

import { useAuth } from '../../hooks/useAuth';
import { useTheme } from '../../hooks/useTheme';
import { utilsStyles } from '../../utils/styles';
import DrowerNavigation from '../DrowerNavigation';

import { useStyles } from './styles';

const Header: React.FC = () => {
  const classes = useStyles();
  const { removeTextDecoration } = utilsStyles();
  const { token, setToken } = useAuth();
  const { toggleTheme, isDarkTheme } = useTheme();

  const [drawerNavigation, setDrawerNavigation] = useState(false);

  const logout = () => {
    setToken('');
  };

  return (
    <AppBar elevation={0} position="relative" color="transparent">
      <Container maxWidth="md">
        <Toolbar disableGutters className={classes.header}>
          <Link to="/" className={removeTextDecoration}>
            <Button variant="text">
              <img src="logo.png" alt="Logo" className={classes.logo} />
            </Button>
          </Link>
          {token && (
            <>
              <Hidden xsDown>
                <Box display="flex" flex="1" justifyContent="space-evenly">
                  <Link to="/dashboard" className={removeTextDecoration}>
                    <LinkStyle component="div">
                      <Typography>Dashboard</Typography>
                    </LinkStyle>
                  </Link>
                  <Link to="/client-register" className={removeTextDecoration}>
                    <LinkStyle component="div">
                      <Typography>Register</Typography>
                    </LinkStyle>
                  </Link>
                </Box>
              </Hidden>
              <Hidden smUp>
                <IconButton
                  className={classes.menuOutlinedButton}
                  color="primary"
                  onClick={() => setDrawerNavigation(true)}
                >
                  <MenuOutlinedIcon />
                </IconButton>
              </Hidden>
            </>
          )}
          <IconButton
            className={classes.toggleThemeButton}
            color="primary"
            onClick={() => toggleTheme()}
          >
            {isDarkTheme ? <SunIcon /> : <MoonIcon />}
          </IconButton>
          {token && (
            <Hidden xsDown>
              <Button variant="outlined" onClick={logout}>
                Logout
                <ExitToAppOutlinedIcon fontSize="inherit" />
              </Button>
            </Hidden>
          )}
        </Toolbar>

        <DrowerNavigation
          open={drawerNavigation}
          onClose={() => setDrawerNavigation(false)}
          logout={logout}
        />
      </Container>
    </AppBar>
  );
};

export default Header;
