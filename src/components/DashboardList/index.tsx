import React, { useContext } from 'react';

import { Box } from '@material-ui/core';

import { ClientContext } from '../../context/clientList';
import RowDashboardTable from '../RowDashboardTable';
import SearchInput from '../SearchInput';

const DashList: React.FC = () => {
  const { list } = useContext(ClientContext);
  return (
    <>
      <SearchInput />
      <Box mt={4}>
        {list.map((cardElement, index) => (
          <RowDashboardTable {...cardElement} key={index} />
        ))}
      </Box>
    </>
  );
};

export default DashList;
