import React from 'react';

import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import Close from '@material-ui/icons/Close';

import { useStyles } from './styles';

interface Props {
  onNavigateBack: () => void;
  onClose: () => void;
  title: string;
}

export const HeaderDrawer: React.FC<Props> = ({
  onNavigateBack,
  onClose,
  title,
}: Props) => {
  const styles = useStyles();

  return (
    <Box
      display="flex"
      justifyContent="space-between"
      alignItems="center"
      className={styles.header}
    >
      <Button size="small" onClick={onNavigateBack} color="inherit">
        <ArrowBackIosIcon />
      </Button>
      <Typography variant="body1">{title}</Typography>
      <Button size="small" onClick={onClose} color="inherit">
        <Close />
      </Button>
    </Box>
  );
};
