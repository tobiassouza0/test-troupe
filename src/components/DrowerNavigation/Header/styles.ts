import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(({ palette, shape }) => ({
  header: {
    backgroundColor: palette.primary.main,
    color: palette.grey[50],
    borderEndEndRadius: shape.borderRadius * 2,
    borderEndStartRadius: shape.borderRadius * 2,
    minHeight: 70,
  },
}));
