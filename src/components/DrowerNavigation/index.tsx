import React from 'react';

import { Drawer, Box, Link as LinkStyle, Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';

import { utilsStyles } from '../../utils/styles';

import { HeaderDrawer } from './Header';
import { useStyles } from './styles';

interface IProps {
  open: boolean;
  onClose: () => void;
  logout: () => void;
}

const DrowerNavigation: React.FC<IProps> = ({ open, onClose, logout }) => {
  const classes = useStyles();
  const { removeTextDecoration } = utilsStyles();

  const logoutCloseDrower = () => {
    logout();
    onClose();
  };

  return (
    <Drawer
      classes={{ paperAnchorLeft: classes.drawerContainer }}
      anchor="left"
      open={open}
      onClose={onClose}
    >
      <HeaderDrawer
        onNavigateBack={onClose}
        onClose={onClose}
        title="Navegação"
      />
      <Box p={2}>
        <Link
          to="/dashboard"
          className={removeTextDecoration}
          onClick={onClose}
        >
          <LinkStyle component="div">
            <Typography variant="h5" gutterBottom align="center">
              Dashboard
            </Typography>
          </LinkStyle>
        </Link>
        <Link
          to="/client-register"
          className={removeTextDecoration}
          onClick={onClose}
        >
          <LinkStyle component="div">
            <Typography variant="h5" align="center">
              Register
            </Typography>
          </LinkStyle>
        </Link>
        <Box display="flex" justifyContent="center">
          <LinkStyle component="button" onClick={logoutCloseDrower}>
            <Typography variant="h5" align="center">
              Logout
            </Typography>
          </LinkStyle>
        </Box>
      </Box>
    </Drawer>
  );
};

export default DrowerNavigation;
