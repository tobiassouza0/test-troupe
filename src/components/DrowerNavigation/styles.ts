import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(({ breakpoints }) => ({
  drawerContainer: {
    [breakpoints.down('sm')]: {
      width: '70%',
    },
    maxWidth: 469,
  },
}));
