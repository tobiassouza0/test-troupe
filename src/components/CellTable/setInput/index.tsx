import React from 'react';

import { FormikProps } from 'formik';

import TextInput from '../../TextInput';
import TextInputMask from '../../TextInputMask';

interface IProps<T> {
  name: string;
  placeholder: string;
  label: string;
  formik: FormikProps<T>;
  mask?: string;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const Cell: React.FC<IProps<any>> = ({
  mask,
  name,
  placeholder,
  label,
  formik,
}) => {
  return (
    <>
      {mask ? (
        <TextInputMask
          mask={mask}
          name={name}
          placeholder={placeholder}
          label={label}
          formik={formik}
        />
      ) : (
        <TextInput
          name={name}
          placeholder={placeholder}
          label={label}
          formik={formik}
        />
      )}
    </>
  );
};

export default Cell;
