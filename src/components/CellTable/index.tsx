import React from 'react';

import { Box, Typography } from '@material-ui/core';
import { FormikProps } from 'formik';

import SetInput from './setInput';

interface IProps<T> {
  title: string;
  text: string;
  edit: boolean;
  mask?: string;
  name: string;
  placeholder: string;
  label: string;
  formik: FormikProps<T>;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const Cell: React.FC<IProps<any>> = ({
  title,
  text,
  edit,
  mask,
  name,
  placeholder,
  label,
  formik,
}) => {
  return (
    <Box display="flex" alignItems="center" flexDirection="column">
      <Typography variant="body2">
        <b>{title}</b>
      </Typography>
      {edit ? (
        <SetInput
          mask={mask}
          name={name}
          placeholder={placeholder}
          label={label}
          formik={formik}
        />
      ) : (
        <Typography variant="subtitle2">{text}</Typography>
      )}
    </Box>
  );
};

export default Cell;
