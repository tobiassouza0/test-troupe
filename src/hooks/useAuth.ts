import { useContext, useEffect } from 'react';

import { AuthContext } from '../context/auth';

export interface IProviderTheme {
  token: string;
  setToken: (token: string) => void;
}

export const useAuth = (): IProviderTheme => {
  const { token, setToken: contentSetToken } = useContext(AuthContext);

  useEffect(() => {
    localStorage.setItem('token', token);
  }, [token]);

  const setToken = (token: string) => {
    contentSetToken(token);
  };

  return {
    token,
    setToken,
  };
};
