import { useContext, useEffect } from 'react';

import { ThemeContext } from '../context/theme';

export interface IProviderTheme {
  isDarkTheme: boolean;
  toggleTheme: () => void;
}

export const useTheme = (): IProviderTheme => {
  const { isDarkTheme, toggleTheme: contentToggleTheme } =
    useContext(ThemeContext);

  useEffect(() => {
    localStorage.setItem('theme', isDarkTheme ? 'dark' : 'light');
  }, [isDarkTheme]);

  const toggleTheme = () => {
    contentToggleTheme(!isDarkTheme);
  };

  return {
    isDarkTheme,
    toggleTheme,
  };
};
