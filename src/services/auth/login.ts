import Api from '../../config/api';
import Endpoints from '../../config/endpoints';

export interface ErrorData {
  message: string;
  status: number;
}

export interface User {
  password: string;
  email: string;
}

export const getUser = (): Promise<User> => {
  return new Promise(async (resolve, reject) => {
    try {
      const { data } = await Api.get<Array<User>>(Endpoints.users.login);
      return resolve(data[0]);
    } catch (error) {
      const response: ErrorData = { message: '', status: 500 };
      if (error.response) {
        response.message = error.response.statusText;
        response.status = error.response.status;
      }

      return reject(response);
    }
  });
};

export default getUser;
