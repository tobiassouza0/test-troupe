import axios from 'axios';

const ViaCep = axios.create({
  baseURL: 'http://viacep.com.br/ws/',
});

export interface ErrorData {
  message: string;
  status: number;
}

export interface AddressData {
  logradouro: string;
  bairro: string;
  localidade: string;
  erro?: boolean;
}

export const getAddressData = (cep: string): Promise<AddressData> => {
  return new Promise(async (resolve, reject) => {
    try {
      const { data } = await ViaCep.get<AddressData>(`${cep}/json`);
      return resolve(data);
    } catch (error) {
      const response: ErrorData = { message: '', status: 500 };
      if (error.response) {
        response.message = error.response.statusText;
        response.status = error.response.status;
      }

      return reject(response);
    }
  });
};
