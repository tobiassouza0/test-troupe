import Api from '../../config/api';
import Endpoints from '../../config/endpoints';

export interface ErrorData {
  message: string;
  status: number;
}

export interface Client {
  nome: string;
  cpf: string;
  email: string;
  endereco: {
    cep: number;
    rua: string;
    numero: number;
    bairro: string;
    cidade: string;
  };
  id: number;
}

export interface Clients {
  data: Array<Client>;
}

export const getClients = (): Promise<Array<Client>> => {
  return new Promise(async (resolve, reject) => {
    try {
      const { data } = await Api.get<Array<Client>>(
        Endpoints.client.getClients
      );
      return resolve(data);
    } catch (error) {
      const response: ErrorData = { message: '', status: 500 };
      if (error.response) {
        response.message = error.response.statusText;
        response.status = error.response.status;
      }

      return reject(response);
    }
  });
};

export const getClientsByName = (name: string): Promise<Array<Client>> => {
  return new Promise(async (resolve, reject) => {
    try {
      const { data } = await Api.get<Array<Client>>(
        `${Endpoints.client.getClients}?q=${name}`
      );
      return resolve(data);
    } catch (error) {
      const response: ErrorData = { message: '', status: 500 };
      if (error.response) {
        response.message = error.response.statusText;
        response.status = error.response.status;
      }

      return reject(response);
    }
  });
};
