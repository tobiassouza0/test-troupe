import Api from '../../config/api';
import Endpoints from '../../config/endpoints';

export interface ErrorData {
  message: string;
  status: number;
}

export interface NewClient {
  nome: string;
  cpf: string;
  email: string;
  endereco: {
    cep: number;
    rua: string;
    numero: number;
    bairro: string;
    cidade: string;
  };
}

export const addClient = (client: NewClient): Promise<NewClient> => {
  return new Promise(async (resolve, reject) => {
    try {
      const { data } = await Api.post<NewClient>(
        Endpoints.client.addClient,
        client
      );
      return resolve(data);
    } catch (error) {
      const response: ErrorData = { message: '', status: 500 };
      if (error.response) {
        response.message = error.response.statusText;
        response.status = error.response.status;
      }

      return reject(response);
    }
  });
};

export default addClient;
