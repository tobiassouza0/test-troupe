import Api from '../../config/api';
import Endpoints from '../../config/endpoints';

export interface ErrorData {
  message: string;
  status: number;
}

interface Client {
  nome: string;
  cpf: string;
  email: string;
  endereco: {
    cep: number;
    rua: string;
    numero: number;
    bairro: string;
    cidade: string;
  };
  id: number;
}

export const editClient = (client: Client): Promise<Client> => {
  return new Promise(async (resolve, reject) => {
    try {
      const { data } = await Api.put(
        Endpoints.client.editClient(client.id),
        client
      );
      return resolve(data);
    } catch (error) {
      const response: ErrorData = { message: '', status: 500 };
      if (error.response) {
        response.message = error.response.statusText;
        response.status = error.response.status;
      }

      return reject(response);
    }
  });
};

export default editClient;
