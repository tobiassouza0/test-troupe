import Api from '../../config/api';
import Endpoints from '../../config/endpoints';

export interface ErrorData {
  message: string;
  status: number;
}

export const deleteClient = (id: number): Promise<unknown> => {
  return new Promise(async (resolve, reject) => {
    try {
      const { data } = await Api.delete(Endpoints.client.deleteClient(id));
      return resolve(data);
    } catch (error) {
      const response: ErrorData = { message: '', status: 500 };
      if (error.response) {
        response.message = error.response.statusText;
        response.status = error.response.status;
      }

      return reject(response);
    }
  });
};

export default deleteClient;
