import React, { createContext, SetStateAction } from 'react';

export interface Client {
  name: string;
  cpf: string;
  email: string;
  city: string;
  id: number;
  cep: number;
  rua: string;
  numero: number;
  bairro: string;
}

interface ArrayClient {
  list: Array<Client>;
  setList: React.Dispatch<SetStateAction<Array<Client>>>;
}

export const ClientContext = createContext<ArrayClient>({
  list: [],
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setList: () => {},
});

export const ProviderClient: React.FC<ArrayClient> = ({
  children,
  list,
  setList,
}) => {
  const value: ArrayClient = {
    list,
    setList,
  };

  return (
    <ClientContext.Provider value={value}>{children}</ClientContext.Provider>
  );
};
