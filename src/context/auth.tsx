import React, { createContext, SetStateAction } from 'react';

interface IProviderAuth {
  token: string;
  setToken: React.Dispatch<SetStateAction<string>>;
}

export const AuthContext = createContext<IProviderAuth>({
  token: '',
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setToken: () => {},
});

export const ProviderAuth: React.FC<IProviderAuth> = ({
  children,
  token,
  setToken,
}) => {
  const value: IProviderAuth = {
    token,
    setToken,
  };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};
