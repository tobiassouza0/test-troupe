import React, { createContext, SetStateAction } from 'react';

interface IProviderTheme {
  isDarkTheme: boolean;
  toggleTheme: React.Dispatch<SetStateAction<boolean>>;
}

export const ThemeContext = createContext<IProviderTheme>({
  isDarkTheme: false,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  toggleTheme: () => {},
});

export const ProviderTheme: React.FC<IProviderTheme> = ({
  children,
  isDarkTheme,
  toggleTheme,
}) => {
  const value: IProviderTheme = {
    isDarkTheme,
    toggleTheme,
  };

  return (
    <ThemeContext.Provider value={value}>{children}</ThemeContext.Provider>
  );
};
