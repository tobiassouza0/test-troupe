# Clients Overview

## Instalação das dependências

```
npm install
```

## Environment

Colocar no .env `REACT_APP_API_BASE` que é o link do servidor para que possam ser feitas as requisições o padrão é `http://localhost:5000`.

## Execução do projeto

Para executar o projeto deixe o servidor ouvindo requisições:

```
npm run server
```

E execute o front end:

```
npm start
```
